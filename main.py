
import json
import time

tweet_counter = {}
max_likes = {}

def main():
    f = open('data.json')

    # returns JSON object as
    # a dictionary
    data = json.load(f)


    # Iterating through the json
    # list
    for i in data['tweets']:

        # simulate events by sleeping
        time.sleep(1)

        name = i['name']
        loc = i['location']
        likes = int(i['likes'])
        text = i['text']
        if name == '':
            print("Wrong data input. Name cannot be empty")
            continue
        elif loc == '':
            loc = "Unknown"
        elif likes < 0:
            print("Likes cannot be less than 0.")
            likes = 0

        # 3 conditions
        print_location(name, loc)
        num_tweets(name)
        num_likes(name, likes, text)

    # Closing file
    f.close()

# print location from where tweet generated
def print_location(name, loc):
    print(name + " tweeted from: " + loc)

# keep count of tweets from each account
def num_tweets(name):
    if name in tweet_counter.keys():
        c = tweet_counter[name]
        tweet_counter[name] = c + 1
    else:
        tweet_counter[name] = 1
        print("Congrats " + name + " you made your first tweet.")

    if tweet_counter[name] % 10 == 0:
        print("Condition of 10 tweets was met by: " + name)

# notify user on their most popular tweet
# can be extended to use retweets as well
def num_likes(name, likes, tweet):
    if name in max_likes.keys():
        c = max_likes[name]
        if likes > c:
            max_likes[name] = likes
            print("Awesome " + name +" you broke your likes record!!! Keep it up.")
            most_liked(name, tweet)
    else:
        max_likes[name] = likes
        print("Congrats " + name + " you got your first like.")

# most popular tweet on the platform
def most_liked(name, tweet):
    max_key = max(max_likes, key=max_likes.get)
    if max_key == name:
        print("Congrats " + name + " this is the most liked tweet currently: " + tweet)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

